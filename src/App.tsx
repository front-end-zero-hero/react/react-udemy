import './App.css'
import {ReactElement} from "react";
import {Header} from "./components/Header/Header.tsx";
import CoreConcepts from "./components/CoreConcept/CoreConcepts.tsx";
import Examples from "./components/Examples/Examples.tsx";

const App = (): ReactElement => {


    return (
        <>
            <Header/>
            <main>
                <CoreConcepts/>
                <Examples/>
            </main>
        </>
    )
}

export default App
