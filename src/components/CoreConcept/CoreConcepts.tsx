import {CORE_CONCEPTS} from "../../data.ts";
import {CoreConcept} from "./CoreConcept.tsx";

const CoreConcepts = () => {
    return (
        <section id="core-concepts">
            <h2>
                Core Concepts
            </h2>
            <ul>
                {
                    CORE_CONCEPTS.map((conceptItem, index) => (<CoreConcept key={index} {...conceptItem} />))
                }
            </ul>
        </section>
    )
}

export default CoreConcepts;