import React, {ReactElement} from "react";
import './CoreConcet.css';
export const CoreConcept: React.FC<{title: string, description: string, image: string}> =
    ({title, description, image}: {title: string, description: string, image: string}): ReactElement => {
        return (
            <li>
                <img src={image} alt={title}/>
                <h3>{title}</h3>
                <p>{description}</p>
            </li>
        )
    }