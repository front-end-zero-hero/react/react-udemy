import {TabButton} from "../TabButton/TabButton.tsx";
import {useState} from "react";
import {EXAMPLES} from "../../data.ts";
import Section from "../Section/Section.tsx";
import Tabs from "../Tab/Tabs.tsx";

const Examples = () => {
    const [selectedTopic, setSelectedTopic] = useState<string>();

    const handleSelect = (selectedButton: string) => {
        // selectedButton => 'component', 'jsx', 'props', 'state'
        setSelectedTopic(selectedButton);
        console.log(selectedTopic);
    }

    let tabContent: JSX.Element = <p>Please select a topic</p>;
    if (selectedTopic) {
        tabContent = (
            <div id="tab-content">
                <h3>{EXAMPLES[selectedTopic].title}</h3>
                <p>{EXAMPLES[selectedTopic].description}</p>
                <pre>
                    <code>
                        {EXAMPLES[selectedTopic].code}
                    </code>
                </pre>
            </div>
        )
    }

    const button = <>
        <TabButton onSelect={() => handleSelect('components')}
                   isSelected={selectedTopic === 'components'}>Components</TabButton>
        <TabButton onSelect={() => handleSelect('jsx')}
                   isSelected={selectedTopic === 'jsx'}>JSX</TabButton>
        <TabButton onSelect={() => handleSelect('props')}
                   isSelected={selectedTopic === 'props'}>Props</TabButton>
        <TabButton onSelect={() => handleSelect('state')}
                   isSelected={selectedTopic === 'state'}>State</TabButton>
    </>

    return (
        <>
            <Section title={"Example"} id="examples">
                <Tabs button={button} buttonContainer={"menu"}>{tabContent}</Tabs>
                <h2>Examples</h2>
                <menu>

                </menu>
            </Section>
            <h2>Time to get started!</h2>
        </>
    )
}

export default Examples;
