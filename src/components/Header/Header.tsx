import {ReactElement} from "react";
import reactImg from "../../assets/react-core-concepts.png";
import './Header.css';

export const reactDescriptions = ['Fundamental', 'Crucial', 'Core'];

export const getRandomInt = (max: number) => {
    return Math.floor(Math.random() * (max + 1));
}
export const Header = (): ReactElement => {
    const description = reactDescriptions[getRandomInt(2)];
    return (
        <header>
            <img src={reactImg} alt="Stylized atom" />
            <h1>React Essentials</h1>
            <p>
                {description} React concepts you will need for almost any app you are
                going to build!
            </p>
        </header>
    )
}