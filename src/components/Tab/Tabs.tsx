import {ReactNode} from "react";

interface IProps {
    children: ReactNode,
    button: any,
    buttonContainer: any
}

const Tabs = ({children, button, buttonContainer = "menu"} : IProps) => {
    const ButtonContainer = buttonContainer;
    return (
        <>
            <ButtonContainer>
                {button}
            </ButtonContainer>
            {children}
        </>
    )
}

export default Tabs;