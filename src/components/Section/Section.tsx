import React, {ReactNode} from "react";

interface IProps {
    title: string;
    children: ReactNode;
    id: string;
}

const Section: React.FC<IProps> = ({title, children, id}) => {
    return (
        <section id={id}>
            <h2>{title}</h2>
            {children}
        </section>
    )
}

export default Section;