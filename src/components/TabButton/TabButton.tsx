import {ReactNode} from "react";

type IProps = {
    children: ReactNode,
    onSelect: () => void;
    isSelected: boolean;
}

export const TabButton = ({children, onSelect, isSelected}: IProps) => {
    return <li>
        <button className={isSelected ? "active": undefined} onClick={onSelect}>{children}</button>
    </li>
}